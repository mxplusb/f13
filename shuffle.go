package f13

import (
	"math/rand"
	"sync"
)

// The face value and how many of each card.
const (
	ONE        = 3
	THREE      = 2
	FOUR       = 2
	FIVE       = 3
	SEVEN      = 3
	FRIDAY     = 8
	FRIDAY_VAL = 4
)

// shuffleCards implements the Fisher-Yates shuffle algorithm to truly shuffle the deck. More information can be found
// here: https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
func (g *Game) shuffleCards() {
	n := len(g.Deck.Cards)
	for i := 0; i < n; i++ {
		r := rand.Intn(n - i)
		g.Deck.Cards[r], g.Deck.Cards[i] = g.Deck.Cards[i], g.Deck.Cards[r]
	}
}

func (d *Deck) buildDeck() {
	ch := make(chan Card, 1)
	suits := []string{"Black Cat", "Under the Ladder", "Broken Mirror", "Friday the 13th"}
	var wg sync.WaitGroup

	go func() {
		wg.Wait()
		close(ch)
	}()

	for _, i := range suits {
		go buildSuit(i, 1, ONE, ch, &wg)
		go buildSuit(i, 3, THREE, ch, &wg)
		go buildSuit(i, 4, FOUR, ch, &wg)
		go buildSuit(i, 5, FIVE, ch, &wg)
		go buildSuit(i, 7, SEVEN, ch, &wg)
		go buildSuit(i, FRIDAY_VAL, FRIDAY, ch, &wg)
	}

	for elem := range ch {
		d.Cards = append(d.Cards, elem)
	}
}

func buildSuit(s string, v int, c int, ch chan <- Card, wg *sync.WaitGroup) {
	wg.Add(1)
	for i := 0; i < c; i++ {
		t := Card{
			Suit:  s,
			Value: v,
		}
		ch <- t
	}
	wg.Done()
}
