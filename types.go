package f13

// Player defines the player of the game.
type Player struct {
	Hand []Card
	Score int
}

// Card defines the type of card in use.
type Card struct {
	Value int
	Suit  string
}

type Deck struct {
	Cards []Card
}

type Game struct {
	Round      int
	Deck       Deck
	NumPlayers int
	Players    []Player
}